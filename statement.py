import cv2
import numpy as np
import os
import imutils
from collections import Counter
from tensorflow.keras.models import load_model

# Constants
IMG_SIZE = 96
nb_classes = 26  # 26 classes for the alphabet
MODEL_NAME = 'handsign.h5'

# Labels for each class (A-Z)
out_label = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')

# Check if the pre-trained model exists
if os.path.exists(f'{MODEL_NAME}'):
    model = load_model(MODEL_NAME)  # Load the Keras model
    print('Model loaded successfully!')

# Initialize variables
pre = []  # Keep track of predictions
s = ''    # Keep track of the current word
aWeight = 0.5   # Running average weight for background subtraction
num_frames = 0  # Frame counter
prediction_count = {}  # To count how often each letter is predicted consecutively

# Camera setup
camera = cv2.VideoCapture(0)

# Start real-time video capture
while True:
    grabbed, frame = camera.read()
    if not grabbed:
        break

    # Resize and flip the frame to avoid the mirror effect
    frame = imutils.resize(frame, width=700)
    frame = cv2.flip(frame, 1)
    clone = frame.copy()

    # Get the height and width of the frame
    (height, width) = frame.shape[:2]

    # Dynamically calculate ROI coordinates based on the frame size
    roi_top = int(height * 0.2)    # 20% from the top
    roi_bottom = int(height * 0.7) # 70% from the top
    roi_right = int(width * 0.7)   # 70% from the left
    roi_left = int(width * 0.3)    # 30% from the left

    # Get the ROI for hand detection
    roi = frame[roi_top:roi_bottom, roi_left:roi_right]

    # Convert the ROI to grayscale and blur it
    gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (7, 7), 0)

    # Prepare the image for prediction by resizing and reshaping
    img = cv2.resize(gray, (IMG_SIZE, IMG_SIZE))
    img = img.reshape(1, IMG_SIZE, IMG_SIZE, 1)

    # Make predictions using the pre-trained model
    model_out = model.predict(img)[0]
    prediction_index = np.argmax(model_out)
    prediction_label = out_label[prediction_index]

    # Count how many times the label is predicted consecutively
    if prediction_label not in prediction_count:
        prediction_count[prediction_label] = 1
    else:
        prediction_count[prediction_label] += 1

    # If the same label is predicted more than 10 times, add it to the statement
    if prediction_count[prediction_label] > 30:
        s += prediction_label
        print(f"Added '{prediction_label}' to the statement: {s}")
        # Reset the count for this label after adding it to the statement
        prediction_count = {}

    # Display the current prediction
    cv2.putText(clone, f'{prediction_label}', (450, 150), cv2.FONT_HERSHEY_PLAIN, 5, (0, 255, 0), 2)

    # Display the built word so far
    cv2.putText(clone, f'Word: {s}', (10, 60), cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 0), 2)

    # Draw the dynamically calculated ROI rectangle
    cv2.rectangle(clone, (roi_left, roi_top), (roi_right, roi_bottom), (0, 255, 0), 2)

    # Display the resulting frame
    cv2.imshow("Video Feed", clone)

    # Handle keypresses for quitting
    keypress = cv2.waitKey(1) & 0xFF
    if keypress == ord('q') or keypress == 27:  # Press 'q' or 'ESC' to exit
        break

# Release the camera and close windows
camera.release()
cv2.destroyAllWindows()