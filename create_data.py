import cv2
import numpy as np
import os
from random import shuffle

path = '/Users/sakshirajpura/PycharmProjects/pythonProject/data'
IMG_SIZE = 96


def create_train_data():
    images = []  # List to store image arrays
    labels = []  # List to store labels
    label_dict = {}  # Dictionary to map folder names to numeric labels

    # Get a sorted list of directory names (A to Z)
    dirnames = sorted(os.listdir(path))

    for label, dirname in enumerate(dirnames):
        dirpath = os.path.join(path, dirname)
        if os.path.isdir(dirpath):  # Ensure it's a directory
            for filename in os.listdir(dirpath):
                if filename == '.DS_Store':  # Skip .DS_Store files
                    continue

                filepath = os.path.join(dirpath, filename)
                img = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
                if img is not None:
                    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
                    images.append(np.array(img))
                    labels.append(label)  # Assign the current label
                else:
                    print(f"Failed to load image: {filepath}")
            label_dict[dirname] = label  # Map folder names to numeric labels
            print(f"Processed {dirname} with label {label}")

    # Shuffle the data
    shuffle_data = list(zip(images, labels))
    shuffle(shuffle_data)
    images, labels = zip(*shuffle_data)

    # Convert lists to numpy arrays
    images = np.array(images)
    labels = np.array(labels)

    # Save the arrays and label dictionary
    np.save('train_images.npy', images)
    np.save('train_labels.npy', labels)
    np.save('label_dict.npy', label_dict)  # Save the label mapping

    return images, labels, label_dict


create_train_data()
