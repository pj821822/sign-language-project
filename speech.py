import cv2
import numpy as np
import imutils
import subprocess
from tensorflow.keras.models import load_model
from gtts import gTTS
import os

# Load the pre-trained model
model = load_model('handsign.h5')
print('Model loaded successfully!')

# Initialize variables
current_string = []
words = []
predicting = True
last_letter = None
letter_streak = 0

# Camera setup
camera = cv2.VideoCapture(0)

def speak(text):
    """Use Google's Text-to-Speech for speaking."""
    tts = gTTS(text=text, lang='en')
    tts.save('temp.mp3')  # Save the spoken text to a temporary file
    subprocess.run(['afplay', 'temp.mp3'])  # Play the temporary file
    os.remove('temp.mp3')  # Remove the file after playing

# Start real-time video capture
while True:
    ret, frame = camera.read()
    if not ret:
        break

    frame = imutils.resize(frame, width=700)
    frame = cv2.flip(frame, 1)  # Flip the frame to correct mirror view
    clone = frame.copy()
    (height, width) = frame.shape[:2]

    # Define the region of interest (ROI) for hand detection
    roi_top, roi_bottom, roi_right, roi_left = int(height * 0.2), int(height * 0.7), int(width * 0.7), int(width * 0.3)
    roi = frame[roi_top:roi_bottom, roi_left:roi_right]
    gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (7, 7), 0)

    # Prepare the ROI for the model prediction
    img = cv2.resize(gray, (96, 96))
    img = np.reshape(img, (1, 96, 96, 1))

    if predicting:
        prediction = model.predict(img)
        prediction_index = np.argmax(prediction)
        prediction_letter = chr(65 + prediction_index)

        if prediction_letter == last_letter:
            letter_streak += 1
        else:
            letter_streak = 1
            last_letter = prediction_letter

        if letter_streak == 30:
            current_string.append(prediction_letter)

        cv2.putText(clone, prediction_letter, (450, 150), cv2.FONT_HERSHEY_PLAIN, 5, (0, 255, 0), 2)
        cv2.putText(clone, ''.join(current_string), (10, 60), cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 0), 2)

    cv2.rectangle(clone, (roi_left, roi_top), (roi_right, roi_bottom), (0, 255, 0), 2)
    cv2.imshow("Video Feed", clone)

    keypress = cv2.waitKey(1) & 0xFF
    if keypress == ord('q'):
        break
    elif keypress == ord(' '):
        current_string.append(' ')
    elif keypress == ord('\t'):
        final_text = ''.join(current_string)
        if final_text:
            speak(final_text)
            words.append(final_text)
            current_string = []

# Cleanup
camera.release()
cv2.destroyAllWindows()