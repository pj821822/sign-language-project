import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from tensorflow.keras.optimizers import Adam
from sklearn.metrics import confusion_matrix, classification_report
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt
import seaborn as sns
import os
from collections import defaultdict
from random import shuffle

# Constants
IMG_SIZE = 96
nb_classes = 28  # Adjust according to the number of your actual classes
MODEL_NAME = 'handsign.h5'
PLOT_DIR = 'plots'
TEST_SAMPLES_PER_CLASS = 100  # We want exactly 100 samples per class in the test set

# Ensure the directory for saving plots exists
os.makedirs(PLOT_DIR, exist_ok=True)

# Load the generated dataset
train_images = np.load('train_images.npy')
train_labels = np.load('train_labels.npy')
label_dict = np.load('label_dict.npy', allow_pickle=True).item()  # Load the label dictionary

# Invert the label_dict for easier access from labels to class names
label_dict_inv = {v: k for k, v in label_dict.items()}

# Organize data by class
data_by_class = defaultdict(list)
for img, label in zip(train_images, train_labels):
    data_by_class[label].append((img, label))

# Split into training and testing sets by ensuring 100 samples per class
train_data = []
test_data = []

for label, data in data_by_class.items():
    shuffle(data)  # Shuffle data within each class
    if len(data) >= TEST_SAMPLES_PER_CLASS:
        test_data += data[:TEST_SAMPLES_PER_CLASS]  # Take 100 samples for the test set
        train_data += data[TEST_SAMPLES_PER_CLASS:]  # The rest go to the training set
    else:
        print(f"Not enough data for label {label}, only {len(data)} samples available.")

# Shuffle the final training and test sets to ensure randomness
shuffle(train_data)
shuffle(test_data)

# Print dataset sizes for debugging
print('Training data length:', len(train_data))
print('Test data length:', len(test_data))

# Prepare dataset for training and testing
X_train = np.array([i[0] for i in train_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
Y_train = np.array([i[1] for i in train_data])
X_test = np.array([i[0] for i in test_data]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
Y_test = np.array([i[1] for i in test_data])

# One-hot encode the labels for training and test data
Y_train = to_categorical(Y_train, num_classes=nb_classes)
Y_test = to_categorical(Y_test, num_classes=nb_classes)

# Print shapes for debugging
print(f"X_train shape: {X_train.shape}")
print(f"Y_train shape: {Y_train.shape}")
print(f"X_test shape: {X_test.shape}")
print(f"Y_test shape: {Y_test.shape}")

# Define the CNN model
def create_model():
    model = Sequential([
        Conv2D(32, (3, 3), activation='relu', input_shape=(IMG_SIZE, IMG_SIZE, 1)),
        MaxPooling2D(pool_size=(2, 2)),
        Dropout(0.25),

        Conv2D(64, (3, 3), activation='relu'),
        MaxPooling2D(pool_size=(2, 2)),
        Dropout(0.25),

        Conv2D(128, (3, 3), activation='relu'),
        MaxPooling2D(pool_size=(2, 2)),
        Dropout(0.25),

        Flatten(),
        Dense(256, activation='relu'),
        Dropout(0.5),
        Dense(nb_classes, activation='softmax')
    ])

    # Compile the model with Adam optimizer and categorical crossentropy loss
    model.compile(optimizer=Adam(learning_rate=1e-3), loss='categorical_crossentropy', metrics=['accuracy'])
    return model

# Create the model
model = create_model()

# Define early stopping to prevent overfitting
early_stopping = EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)

# Train the model with early stopping
history = model.fit(X_train, Y_train, epochs=10, batch_size=32, validation_data=(X_test, Y_test),
                    callbacks=[early_stopping], verbose=1)

# Save the trained model
model.save(MODEL_NAME)

# Evaluate the model on the test set
score = model.evaluate(X_test, Y_test, verbose=0)
print(f'Test accuracy: {score[1] * 100:.4f}%')

# Plotting training and validation accuracy and loss
plt.figure(figsize=(10, 4))
plt.subplot(1, 2, 1)
plt.plot(history.history['accuracy'], label='Training Accuracy')
plt.plot(history.history['val_accuracy'], label='Validation Accuracy')
plt.title('Training and Validation Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()

plt.subplot(1, 2, 2)
plt.plot(history.history['loss'], label='Training Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.title('Training and Validation Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.tight_layout()
plt.savefig(os.path.join(PLOT_DIR, 'training_validation_curves.png'))
plt.show()

# Confusion Matrix and Classification Report
predictions = model.predict(X_test)
predicted_classes = np.argmax(predictions, axis=1)
true_classes = np.argmax(Y_test, axis=1)

# Replace numeric labels with class names from label_dict
predicted_class_names = [label_dict_inv[label] for label in predicted_classes]
true_class_names = [label_dict_inv[label] for label in true_classes]

# Confusion Matrix
cm = confusion_matrix(true_class_names, predicted_class_names, labels=list(label_dict_inv.values()))
plt.figure(figsize=(10, 8))
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=list(label_dict_inv.keys()),
            yticklabels=list(label_dict_inv.keys()))
plt.title('Confusion Matrix')
plt.xlabel('Predicted Labels')
plt.ylabel('True Labels')
plt.savefig(os.path.join(PLOT_DIR, 'confusion_matrix.png'))
plt.show()

# Classification Report
report = classification_report(true_class_names, predicted_class_names, target_names=list(label_dict_inv.keys()))
print('Classification Report:\n', report)
